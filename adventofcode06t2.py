planets = {"COM": 0}
filepath = "a6t2.txt"

with open(filepath) as fp:
    line = fp.readline()
    while line:
        line = line.replace("\n", '')
        line = line.split(")")
        planets[str(line[1])] = str(line[0])
        line = fp.readline()

pathToSan = []
pathToYou = []
startingNode = 'YOU'
while startingNode != "COM":
    pathToYou.append(planets[startingNode])
    startingNode = planets[startingNode]

startingNode = 'SAN'
while startingNode != "COM":
    pathToSan.append(planets[startingNode])
    startingNode = planets[startingNode]
pathToYouCopy = pathToYou
pathToYou = [x for x in pathToYou if x not in pathToSan]
pathToSan = [x for x in pathToSan if x not in pathToYouCopy]
print(len(pathToYou) + len(pathToSan))
