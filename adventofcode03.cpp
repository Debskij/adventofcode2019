//
// Created by kuba on 03.12.2019.
//
#include <iostream>
#include <vector>

using namespace std;

int isColision(pair<pair<int, int>, pair<int, int>> l1,
                pair<pair<int, int>, pair<int, int>> l2){
    // input data: l1 stands for first wire, l2 for second one.
    // First pair in each of those stands for starting point, second stands for end point of each "partial wire"
    // We pass argument as ((x1start, y1start),(x1end, y1end)), ((x2start, y2start),(x2end, y2end))
    // This function returns does pair of partial wire makes a cross, if so we want to return place of cross and add it
    // To vector of pairs collecting positions and distances from 0,0 point
    bool dirl1, dirl2;      //if true its vertical, false means horizontal
    dirl1 = l1.first.first == l1.second.first;
    dirl2 = l2.first.first == l2.second.first;
    if(dirl1 == dirl2)
        return 0;       //two partial wires going same direction
    else{
        // from this moment we dont mind about order that was passed, l1 will be vertical, l2 horizontal for easier operations
        if(dirl2)
            swap(l1, l2);
        if(((l2.second.first <= l1.first.first <= l2.first.first) or (l2.second.first >= l1.first.first >= l2.first.first))
            and
            ((l1.first.second <= l2.first.second <= l1.second.second) or (l1.first.second >= l2.first.second >= l1.second.second))){
            return l1.first.first + l2.first.second;
        }
    }



}

int main(){
    vector<pair<int, int>> w1, w2 = {{0,0}};
    string a;
    while(cin >> a){
        pair <int, int> previousPosition = w1[w1.size()-1];
        if(a[0] == 'U')
            w1.emplace_back(previousPosition.first + stoi(a.erase(0, 1)), previousPosition.second);
        if(a[0] == 'D')
            w1.emplace_back(previousPosition.first - stoi(a.erase(0, 1)), previousPosition.second);
        if(a[0] == 'L')
            w1.emplace_back(previousPosition.first, previousPosition.second - stoi(a.erase(0, 1)));
        if(a[0] == 'R')
            w1.emplace_back(previousPosition.first, previousPosition.second + stoi(a.erase(0, 1)));
        if(a[0] == '0')
            break;
    }
    while(cin >> a){
        pair <int, int> previousPosition = w2[w2.size()-1];
        if(a[0] == 'U')
            w2.emplace_back(previousPosition.first + stoi(a.erase(0, 1)), previousPosition.second);
        if(a[0] == 'D')
            w2.emplace_back(previousPosition.first - stoi(a.erase(0, 1)), previousPosition.second);
        if(a[0] == 'L')
            w2.emplace_back(previousPosition.first, previousPosition.second - stoi(a.erase(0, 1)));
        if(a[0] == 'R')
            w2.emplace_back(previousPosition.first, previousPosition.second + stoi(a.erase(0, 1)));
        if(a[0] == '0')
            break;
    }
    int ans = 0, distance;
    for(unsigned long firstW = 0; firstW < w1.size()-1; ++firstW){
        for(unsigned long secondW = 0; secondW < w2.size()-1; ++secondW){
            distance = isColision({w1[firstW], w1[firstW+1]}, {w2[secondW], w2[secondW+1]});
            ans = ans < distance ? distance : ans;
        }
    }
    cout << ans << endl;

}

