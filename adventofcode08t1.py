import textwrap

size = 25*6

filepath = "a8.txt"

with open(filepath) as fp:
    sometext = fp.read()
    sometext = textwrap.wrap(sometext, size)

minZeros = size
ans = 0
tempZeros = size
for x in sometext:
    tempZeros = x.count('0')
    if tempZeros < minZeros:
        minZeros = tempZeros
        ans = x.count('1') * x.count('2')
        print(x, tempZeros)
print(ans)