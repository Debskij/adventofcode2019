planets = {"COM": 0}
filepath = "a6.txt"

with open(filepath) as fp:
    line = fp.readline()
    while line:
        line = line.replace("\n", '')
        line = line.split(")")
        if line[0] not in planets.keys() or isinstance(planets.__getitem__(line[0]), str):
            planets[str(line[1])] = str(line[0])
        else:
            print(type(line[0]))
            planets[str(line[1])] = int(planets[str(line[0])] + 1)
        line = fp.readline()

while any(isinstance(x, str) for x in planets.values()):
    for key, val in planets.items():
        if isinstance(val, str) and val in planets.keys() and isinstance(planets[val], int):
            planets[key] = int(planets[val] + 1)

print(sum(planets.values()))