import numpy as np
import matplotlib.pyplot as plt

filepath = "a8.txt"

with open(filepath) as fp:
    sometext = fp.read()

sizeX, sizeY = 25, 6
img = np.zeros((sizeY, sizeX), dtype=np.uint8) + 2
for i in range(len(sometext) // (sizeX * sizeY)):
    layer = list(sometext[i * sizeX * sizeY: (i + 1) * sizeX * sizeY])
    nplayer = np.array([int(x) for x in layer], dtype=np.uint8).reshape((sizeY, sizeX))
    np.copyto(img, nplayer, where=(img == 2))

plt.imshow(img, cmap='gray', vmin=0, vmax=2)
plt.show()